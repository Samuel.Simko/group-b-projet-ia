# -*- coding: utf-8 -*-

import numpy as np
import matplotlib.pyplot as plt

class Perceptron(object):
    """A Perceptron neuron

    Attributes
    ----------
    weights : array
        The initial weights of the neuron

    """

    def __init__(self, weights):
        self.__weights = weights

    @property
    def weights(self):
        return self.__weights

    @weights.setter
    def weights(self, weights):
        self.__weights = weights

    def train(self, x, y, learning_rate=0.01, nb_iter=1000):
        for _ in range(nb_iter):
            for i, xi in enumerate(x):
                tmp_y = 1 if np.dot(xi, self.weights) >= 0 else 0
                self.weights += (learning_rate * (y[i] - tmp_y)) * xi

    def predict(self, x):
        return 1 if np.dot(x, self.weights[:-1]) + self.weights[-1] >= 0 else 0

# Initialisation de données (augmentées) aléatoire entre -1 et 1
input_dim = 3
x = np.random.uniform(-1,1, size=(400,input_dim))
x[:,-1] = 1

# Label des données
y = np.array([ 1 if k[0] > -0.5 else 0 for k in x])

# (and)
#y = np.array([ 1 if k[1] > 0 and k[0] >= 0 else 0 for k in x])

# Initialisation des poids aléatoirement et du Perceptron
weights = np.array(0.5 - np.random.random(size=(input_dim)))
p = Perceptron(weights)

# Entrainement du Perceptron avec les données aléatoires
p.train(x, y)

# Affichage des données aléatoires avec la droite de séparation calculée.
plt.subplot(1,2,1)

# w1*x1 + w2*x2 + w3 = 0 => x2 = (-w1*x1 - w3) / w2
# où w3 est le biais
x01, x02 = np.amin(x[:, 0]), np.amax(x[:, 0])
x11 = (-p.weights[0] * x01 - p.weights[-1]) / p.weights[1]
x12 = (-p.weights[0] * x02 - p.weights[-1]) / p.weights[1]
plt.plot([x01, x02], [x11, x12], "k")

ymin, ymax = np.amin(x[:, 1]), np.amax(x[:, 1])
plt.ylim([ymin-1, ymax+1])

for i, xi in enumerate(x):
    if y[i] == 1:
        plt.plot(xi[0], xi[1], 'or')
    else:
        plt.plot(xi[0], xi[1], 'ob')
plt.title("Testing data")

# Affichage de prédiction de données de teste avec le Perceptron entrainée.
x_test = np.array([[i,j] for i in np.linspace(-1, 1, 20) for j in np.linspace(-1, 1, 20)])
plt.subplot(1,2,2)
for [i,j] in x_test:
    if p.predict([i,j]):
        plt.plot(i, j, 'or')
    else:
        plt.plot(i, j, 'ob')
plt.title("Perceptron result")

plt.plot([x01, x02], [x11, x12], "k")
plt.ylim([ymin-1, ymax+1])

plt.show()
