# -*- coding: utf-8 -*-
import random

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import preprocessing
from neuneu import *

# open mushroom database

data = preprocessing.normalize(pd.read_csv("winequality-white.csv", delimiter = ";").values, axis = 0)
np.insert(data, 0, data[:,-1], axis = 1)
np.delete(data, -1, 1)
np.random.shuffle(data)

# replace each letter by a double from 0-1

N = data.shape[0]

training_data = data[: int(0.7 * N)]
training_labels = training_data[:, 0]
training_labels = [[x] for x in training_labels]
training_data = np.delete(training_data, 0, 1)

testing_data = data[int(0.7 * N) :]
testing_labels = testing_data[:, 0]
testing_labels = [[x] for x in testing_labels]
testing_data = np.delete(testing_data, 0, 1)


nn = Neural_network()
nn.add_layer(Layer(4, "sigmoid", training_data.shape[1]))
nn.add_layer(Layer(5, "sigmoid"))
nn.add_layer(Layer(1, "sigmoid"))

NUM_EPOCHS = 20
for i in range(NUM_EPOCHS):
    print(f"Epoch {i+1}/{NUM_EPOCHS}")
    nn.train(training_data, training_labels, learning_rate=0.03)
    print(
        f"Loss: {np.mean([(np.array(nn.predict(testing_data[k])) - testing_labels[k][0])**2 for k in range(len(testing_labels))])}"
    )
