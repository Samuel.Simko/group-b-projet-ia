# -*- coding: utf-8 -*-
import random
import time
import tqdm

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.neural_network import MLPClassifier
from battelle.nn import *

import battelle.knn as knn


# Open white wine database
data = pd.read_csv("winequality-red.csv", delimiter=";").values

# Rescale data so that each feature has mean 0 and std 1
scaler = preprocessing.MinMaxScaler()
scaler.fit(data)
data_scaled = scaler.transform(data)

np.random.shuffle(data_scaled)


# Define training (70%) and testing (30%) data
N = data.shape[0]

training_data = data_scaled[: int(0.8 * N)]
training_labels = (10*training_data[:, -1]).astype(int)
training_labels_onehot = np.zeros((training_labels.size, 11))
training_labels_onehot[np.arange(training_labels.size), training_labels] = 1
training_data = training_data[:, :-1]

print(training_labels_onehot)
# training_labels = [x for x in training_labels]

testing_data = data_scaled[int(0.2 * N) :]
testing_labels = (10*testing_data[:, -1]).astype(int)
testing_labels_onehot = np.zeros((testing_labels.size, 11))
testing_labels_onehot[np.arange(testing_labels.size), testing_labels] = 1
testing_data = testing_data[:, :-1]


# Define neural network

NB_FEATURES = training_data.shape[1]

print(training_labels, NB_FEATURES)

nn = NeuralNetwork()
nn.add_layer(Layer(5, "tanh", NB_FEATURES))
nn.add_layer(Layer(4, "tanh"))
nn.add_layer(Layer(1, "tanh"))

print(training_labels)
# clf = MLPClassifier(hidden_layer_sizes=(0), activation='tanh', learning_rate='constant', learning_rate_init=0.04,
clf = MLPClassifier(hidden_layer_sizes=(4), activation='tanh', max_iter=1000000
                    ).fit(training_data, list(training_labels))

clfpred = np.array(clf.predict(testing_data)) - testing_labels
clf_accuracy = np.count_nonzero(clfpred==0)/len(clfpred)
print(clf_accuracy)

knnclass = knn.KNearestNeighborsClassifier()
knnclass.train(training_data, training_labels)

knnpred = np.array(([knnclass.predict(testing_data[k]) - testing_labels[k] for k in range(len(testing_labels))]))
knn_accuracy = np.count_nonzero(knnpred==0)/len(knnpred)
print(knn_accuracy)

# print(knnclass.predict(testing_data[0]))

NB_ITER = 40
NB_TIMES = 3

losses_testing = [[] for _ in range(NB_TIMES)]
losses_training = [[] for _ in range(NB_TIMES)]
epoch = range(NB_ITER)
accuracies_training = [[] for _ in range(NB_TIMES)]
accuracies_testing = [[] for _ in range(NB_TIMES)]

for j in tqdm.trange(NB_TIMES):
    nn = NeuralNetwork()
    nn.add_layer(Layer(5, "tanh", NB_FEATURES))
    nn.add_layer(Layer(4, "tanh",))
    nn.add_layer(Layer(11, "tanh"))
    for i in range(NB_ITER):
        nn.train(training_data, training_labels_onehot, learning_rate=0.03)

        loss = np.mean([(np.array(nn.predict(testing_data[k])) - testing_labels_onehot[k])**2 for k in range(testing_labels_onehot.shape[1])])
        losses_testing[j].append(loss)

        loss = np.mean([(np.array(nn.predict(training_data[k])) - training_labels_onehot[k])**2 for k in range(training_labels_onehot.shape[1])])
        losses_training[j].append(loss)

        print(
            f"Loss: {loss}"
        )
        accuracies_testing[j].append(nn.get_weighted_metrics(testing_data, testing_labels_onehot)["accuracy"])
        accuracies_training[j].append(nn.get_weighted_metrics(training_data, training_labels_onehot)["accuracy"])
        print(i)
        i += 1

losses_training_mean = np.mean(losses_training, axis=0)
plt.subplot(1,2,1)
plt.plot(epoch, losses_training_mean, 'b')

losses_testing_mean = np.mean(losses_testing, axis=0)

plt.plot(epoch, losses_testing_mean, 'g')

plt.legend([r"Moyenne des pertes $\mathcal{L}$ des données d'entraînement",
            r"Moyenne des pertes $\mathcal{L}$ des données de test"])

plt.xlabel("Nombre d'epoch")

ci = np.std(losses_training, axis=0)
print(ci)
plt.fill_between(epoch, (losses_training_mean-ci), (losses_training_mean+ci), color='blue', alpha=0.1)

ci = np.std(losses_testing, axis=0)
print(ci)
plt.fill_between(epoch, (losses_testing_mean-ci), (losses_testing_mean+ci), color='green', alpha=0.1)

plt.subplot(1,2,2)
accuracies_testing_mean = np.mean(accuracies_testing, axis=0)
plt.plot(epoch, accuracies_testing_mean, 'g')

accuracies_training_mean = np.mean(accuracies_training, axis=0)
plt.plot(epoch, accuracies_training_mean, 'b')

plt.xlabel("Nombre d'epoch")

plt.plot([0, NB_ITER], [knn_accuracy, knn_accuracy], 'r--')
plt.plot([0, NB_ITER], [clf_accuracy, clf_accuracy], 'p--')
plt.legend(["Précision du réseau de neurones (données de test)",
            "Précision du réseau de neurones (données d'entraînement)",
            "Meilleure précision du classifieur KNN (données d'entraînement)",
            "Meilleure précision du MLPClassifier (données d'entraînement)"])

ci = np.std(accuracies_training, axis=0)
plt.fill_between(epoch, (accuracies_training_mean-ci), (accuracies_training_mean+ci), color='blue', alpha=0.1)

ci = np.std(accuracies_testing, axis=0)
plt.fill_between(epoch, (accuracies_testing_mean-ci), (accuracies_testing_mean+ci), color='green', alpha=0.1)

plt.show()
