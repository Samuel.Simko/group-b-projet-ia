# -*- coding: utf-8 -*-
import requests

url = "https://archive.ics.uci.edu/ml/machine-learning-databases/wine-quality/"
files = ["winequality-red.csv", "winequality-white.csv"]

for filename in files:
    r = requests.get(url + filename, allow_redirects=True)
    with open(filename, "wb") as f:
        f.write(r.content)
