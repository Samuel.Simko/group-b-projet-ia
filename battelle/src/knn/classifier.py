import numpy as np
import random
from operator import countOf
from numbers import Number

EPSILON = 1e-7


class KNearestNeighborsClassifier(object):
    """Instance of a k-Nearest Neighbors Classifier.

    Parameters
    ----------
    n_neighbors : int, default=5
        Number of neighbors to use for kneighbors queries.

    tiebreaker : {'nearest', 'random'}, default='nearest'
        Methoed used to determine the winner class of the majority voting in case of a tie.
        Possible values:
        - 'nearest' : the nearest neighbor in the tied classes is used to break the tie.
        - 'random' : the winner is chosen randomly among the tied classes.

    weights : {'uniform', 'distance'}, default='uniform'
        Weight function used in prediction.  Possible values:
        - 'uniform' : uniform weights.  All points in each neighborhood
          are weighted equally.
        - 'distance' : weight points by the inverse of their distance.

    p : int, default=2
        Power parameter for the Minkowski metric.

    metric : {'minkowski', 'hamming'}, default='minkowski'
        The distance metric to use if weights is set to 'distance'.
    """

    def __init__(
        self,
        n_neighbors=5,
        tiebreaker="nearest",
        weights="uniform",
        p=2,
        metric="minkowski",
    ):
        self.__n_neighbors = self.__check_n_neighbors(n_neighbors)
        self.__tiebreaker = self.__check_tiebreaker(tiebreaker)
        self.__weights = self.__check_weights(weights)
        self.__p = self.__check_p(p)
        self.__metric = self.__check_metric(metric)

        self.__x = []
        self.__y = []

    def __check_n_neighbors(self, value):
        if np.issubdtype(type(value), np.integer):
            if value > 0:
                return value
            else:
                raise ValueError("n_neighbors should be > 0, got {}".format(value))
        else:
            raise TypeError(
                "n_neighbors should be an integer, got {}".format(type(value))
            )

    def __check_tiebreaker(self, value):
        if isinstance(value, str):
            if value == "nearest" or value == "random":
                return value
            else:
                raise ValueError(
                    'tiebreaker should be either "nearest" or "random", got {}'.format(
                        value
                    )
                )
        else:
            raise TypeError("tiebreaker should be a string, got {}".format(type(value)))

    def __check_weights(self, value):
        if isinstance(value, str):
            if value == "uniform" or value == "distance":
                return value
            else:
                raise ValueError(
                    'weights should be either "uniform" or "distance", got {}'.format(
                        value
                    )
                )
        else:
            raise TypeError("weights should be a string, got {}".format(type(value)))

    def __check_p(self, value):
        if isinstance(value, Number):
            return value
        else:
            raise TypeError("p should be a number, got {}".format(type(value)))

    def __check_metric(self, value):
        if isinstance(value, str):
            if value == "minkowski" or value == "hamming":
                return value
            else:
                raise ValueError(
                    'metric should be either "minkowski" or "hamming", got {}'.format(
                        value
                    )
                )
        else:
            raise TypeError("metric should be a string, got {}".format(type(value)))

    def train(self, x, y):
        """Fit the k-nearest neighbors classifier from the training dataset.

        Note
        ----
        For a k-NN classifier, it only consits of storing the data provided.

        Parameters
        ----------
        x : array-like of shape (n_samples, n_features)
            Training data
        y : array-like of shape (n_samples) or (n_samples, n_outputs)
            Target values
        """
        if len(x) == len(y):
            self.__x = np.array(x)

            y = np.array(y)
            temp_y = []
            if y.ndim == 1:
                for f in y:
                    temp_y.append([f])
                y = np.array(temp_y)
            self.__y = y
            # print('trained y', self.__y)
        else:
            raise ValueError("x and y need to have the same number of samples")

    def __distance(self, x1, x2):
        """Calculate the distance between x1 and x2 according to the classifier's metric.

        Parameters
        ----------
        x1,x2 : array-like of shape (n_features)
            Features for which the distance is calculated

        Returns
        -------
        distance : float
            Distance between x1 and x2 according to the classifier's metric.
        """
        if self.__metric == "minkowski":
            return pow(
                sum(pow(abs(a - b), self.__p) for a, b in zip(x1, x2)), (1 / self.__p)
            )

        elif self.__metric == "hamming":
            unequal = 0
            for a, b in zip(x1, x2):
                unequal += int(a != b)
            return unequal / len(x1)

    def __kneighbors(self, x):
        """Find the k nearest neighbors of point x.

        Parameters
        ----------
        x : array-like of shape (n_features)
            The query point

        Returns
        -------
        indices : ndarray<int> of length n_neighbors
            Indices of the k nearest neighbors of point x
        distances : ndarray<float> of length n_neighbors
            Distances between the k nearest neighbors and point x
        """
        distances = np.array([])
        for xi in self.__x:
            distances = np.append(distances, self.__distance(x, xi))
        n_neighbors = min(len(self.__x), self.__n_neighbors)
        indices = distances.argsort()[:n_neighbors]
        distances = np.sort(distances)[:n_neighbors]
        return indices, distances

    def __majority(self, values, distances):
        """Finds the majority element(s) according to the classifier's metric and tiebreaker method

        Parameters
        ----------
        values : ndarray<Class label> of length n_neighbors
            List of elements in which to find the majority element(s)
        distances : ndarray<float> of length n_neighbors
            List of the distances from the query point and the elements of the array values

        Returns
        -------
        max_v : Class label
            Majority element
        """
        counter = {}
        for v in values:
            counter[v] = 0
        for i, v in enumerate(values):
            counter[v] += (
                1 if self.__weights == "uniform" else 1.0 / (distances[i] + EPSILON)
            )
        max_v = max(counter, key=counter.get)

        if countOf(counter.values(), counter[max_v]) > 1:
            if self.__tiebreaker == "nearest":
                nearest_index = 0
                nearest_dist = np.inf
                for i, v in enumerate(values):
                    if counter[max_v] == counter[v] and distances[i] < nearest_dist:
                        nearest_dist = distances[i]
                        nearest_index = i
                max_v = values[nearest_index]

            elif self.__tiebreaker == "random":
                max_v = random.choice(
                    list(
                        dict(
                            filter(
                                lambda elem: elem[1] == counter[max_v], counter.items()
                            )
                        ).keys()
                    )
                )
        return max_v

    def predict(self, x):
        """Predict the class label for the provided point

        Parameters
        ----------
        x : array-like of shape (n_features)
            The query point

        Returns
        -------
        y : Class label or ndarray<Class label> of shape (n_outputs)
            Class label for point x
        """
        indices, distances = self.__kneighbors(x)
        categories = [self.__y[i] for i in indices]
        y = []
        for output in np.array(categories).transpose():
            y.append(self.__majority(output, distances))
        if len(y) == 1:
            return y[0]
        return y
