# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from copy import deepcopy
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from battelle.nn import *

# open mushroom database

data = pd.read_csv("agaricus-lepiota.data", header=None).values
np.random.shuffle(data)

# Replace binary values of first column from 0 to 1
data[:, 0] = [{"p": 0, "e": 1}[x] for x in data[:, 0]]

# Replace each other letter by their alphabetical order (eg a = 0, b = 1/25... z = 1)
data[:, 1:] = np.vectorize(lambda x: (ord(x) - 97) / 25)(data[:, 1:])
data = preprocessing.normalize(data, axis = 0)
# replace each letter by a double from 0-1

[training_data, testing_data] = train_test_split(data, train_size=0.8, random_state = 2022)


#training_data = data[: int(0.7 * N)]
training_labels = training_data[:, 0]
training_labels = [[x] for x in training_labels]
training_data = np.delete(training_data, 0, 1)

#testing_data = data[int(0.7 * N) :]
testing_labels = testing_data[:, 0]
testing_labels = [[x] for x in testing_labels]
testing_data = np.delete(testing_data, 0, 1)

nn = NeuralNetwork()
nn.add_layer(Layer(4, "sigmoid", training_data.shape[1]))
nn.add_layer(Layer(5, "sigmoid"))
nn.add_layer(Layer(1, "sigmoid"))

old_loss = 200
loss = 100
i = 0
while old_loss / loss > 1.01:
    print(f"Epoch {i+1}/?")
    nn.train(training_data, training_labels, learning_rate=0.03)
    old_loss = deepcopy(loss)
    loss = np.mean([(np.array(nn.predict(testing_data[k])) - testing_labels[k][0])**2 for k in range(len(testing_labels))])
    print(
        f"Loss: {loss}"
    )
    i += 1
nn.print_weights()
a = nn.all_weights
v_abs = np.vectorize(abs)
for j,i in enumerate(a) : 
    plt.matshow(v_abs(i))
    plt.title("Couche " + str(j))