import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm

from battelle.nn import *

nn = NeuralNetwork()
nn.add_layer(Layer(5, 'tanh', 2))
nn.add_layer(Layer(4, 'tanh'))
nn.add_layer(Layer(1, 'tanh'))

# List of features
x = np.random.random(size=(300,2)) - 0.5
x = [list(xi) for xi in x]

# List of corresponding labels (xor)
y = np.array([np.sign(np.prod(k)) for k in x])

# Train the neural network in epochs
NUM_EPOCHS = 100
for i in range(NUM_EPOCHS):
    print(f"Epoch {i+1}/{NUM_EPOCHS}")
    nn.train(x, y, learning_rate=0.03)
    print(f"Loss: {np.mean([(nn.predict(x[k]) - y[k])**2 for k in range(len(x))])}")

# Plot training data
plt.subplot(1,2,1)
for i, xi in enumerate(x):
    if y[i] == 1:
        plt.plot(xi[0], xi[1], 'xr')
    else:
        plt.plot(xi[0], xi[1], 'xb')
plt.title("Testing data")

# Plot neural net result
plt.subplot(1,2,2)
xrange = np.linspace(-0.5, 0.5, 20)
yrange = np.linspace(-0.5, 0.5, 20)
for x in xrange:
    for y in yrange:
        if nn.predict([x, y]) > 0:
            plt.plot(x, y, 'xr')
        else:
            plt.plot(x, y, 'xb')
    
RES = 100
xcontour = np.linspace(-0.5, 0.5, RES)
ycontour = np.linspace(-0.5, 0.5, RES)
z = []
for x in xcontour:
    row = []
    for y in ycontour:
        row.append(nn.predict([x, y]))
    z.append(row)
z = np.transpose(z)
    
plt.contourf(xcontour,ycontour,z, levels = 2, cmap=cm.coolwarm, alpha=0.5)
plt.title("Neural net result")
plt.show()
