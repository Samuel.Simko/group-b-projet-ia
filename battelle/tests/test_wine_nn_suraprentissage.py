# -*- coding: utf-8 -*-
#import random

import numpy as np
import pandas as pd
from copy import deepcopy
import matplotlib.pyplot as plt
from sklearn import preprocessing
from sklearn.model_selection import train_test_split
from battelle.nn import *

# open mushroom database
data = pd.read_csv("../data/winequality-white.csv", delimiter = ";").values
data = preprocessing.normalize(data, axis = 0)
data = np.vstack((deepcopy(data[:,-1]),data.T)).T #Pour mettre les labels dans la première colonne
data = np.delete(data, -1, 1)
#print(data[:,0])

def tout(data, train_size = 0.2) : 
    print(train_size)
    [training_data, testing_data] = train_test_split(data, train_size = train_size, random_state = 2022)
    
    
    #training_data = data[: int(0.7 * N)]
    training_labels = training_data[:, 0]
    training_labels = [[x] for x in training_labels]
    training_data = np.delete(training_data, 0, 1)
    
    #testing_data = data[int(0.7 * N) :]
    testing_labels = testing_data[:, 0]
    testing_labels = [[x] for x in testing_labels]
    testing_data = np.delete(testing_data, 0, 1)
    
    
    nn = NeuralNetwork()
    nn.add_layer(Layer(4, "sigmoid", training_data.shape[1]))
    nn.add_layer(Layer(5, "sigmoid"))
    # nn.add_layer(Layer(11, "tanh"))
    nn.add_layer(Layer(1,"sigmoid"))
    
    #NUM_EPOCHS = 20
    old_loss = 200
    loss = 100
    iteration = 0
    while old_loss / loss > 1.01:
    # for i in range(10) : 
        print(f"Epoch {iteration+1}/?")
        nn.train(training_data, training_labels, learning_rate=0.03)
        old_loss = deepcopy(loss)
        loss = np.mean([(np.array(nn.predict(testing_data[k])) - testing_labels[k][0])**2 for k in range(len(testing_labels))])
        print(
            f"Loss: {loss}"
        )
        iteration += 1
    nn.print_weights()
    a = nn.all_weights
    v_abs = np.vectorize(abs)
    for j,i in enumerate(a) : 
        plt.matshow(v_abs(i))
        plt.title("Wine dataset : Couche " + str(j))
        plt.close()
    
    return [iteration,loss]


possible_lr = [0.3,0.25,0.2,0.15,0.1,0.09,0.08,0.07,0.06,0.05,0.04,0.03,0.02,0.01]
all_iterations = []
all_losses = []
for lr in possible_lr : 
    i,loss = tout(data, train_size = lr)
    all_iterations.append(i)
    all_losses.append(loss)

graph1, plot1 = plt.subplots(1,1)
plot1.plot(possible_lr, all_iterations)
plot1.invert_xaxis()
plot1.set_title("Nombre d'itérations en fonction de la proportion des données d'apprentissage")
plot1.set_xlabel("Proportion des données d'apprentissage")
plot1.set_ylabel("Nombre final d'EPOCH")

graph2, plot2 = plt.subplots(1,1)
plot2.plot(possible_lr, all_losses)
plot2.invert_xaxis()
plot2.set_title("Erreur finale en fonction de la proportion des données d'apprentissage")
plot2.set_xlabel("Proportion des données d'apprentissage")
plot2.set_ylabel("Erreur finale")
plot2.set_yscale("log")

