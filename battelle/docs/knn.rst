.. battelle.knn

.. currentmodule:: battelle.knn

K-nearest Neighbors Classifier subpackage (battelle.knn)
=========================================================

.. autosummary::
   :toctree: generated/

   KNearestNeighborsClassifier

   
