# -*- coding: utf-8 -*-
import random

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from neuneu import *

nb = 5
accuracy = [[] for _ in range(nb)]
precision= [[] for _ in range(nb)]
recall = [[] for _ in range(nb)]
f1_score = [[] for _ in range(nb)]

for k in range(nb):
    print("-----------------------------------------------------------------------------------------------")
    print(k)

    # open mushroom database
    data = pd.read_csv("agaricus-lepiota.data", header=None).values
    np.random.shuffle(data)

    # Replace binary values of first column from 0 to 1
    data[:, 0] = [{"p": 0, "e": 1}[x] for x in data[:, 0]]

    # Replace each other letter by their alphabetical order (eg a = 0, b = 1/25... z = 1)
    data[:, 1:] = np.vectorize(lambda x: (ord(x) - 97) / 25)(data[:, 1:])

    # replace each letter by a double from 0-1

    N = data.shape[0]

    NUM_EPOCHS = 20

    stds = [10**i for i in np.arange(-3,1,0.5)]
    stds.insert(0,0)

    for std in stds:

        training_data = data[: int(0.7 * N)]
        training_labels = training_data[:, 0]
        training_labels = [[x] for x in training_labels]
        training_data = np.delete(training_data, 0, 1)

        # Add normal noise
        training_data += np.random.normal(scale=std ,size=training_data.shape)

        testing_data = data[int(0.7 * N) :]
        testing_labels = testing_data[:, 0]
        testing_labels = [[x] for x in testing_labels]
        testing_data = np.delete(testing_data, 0, 1)

        nn = Neural_network()
        nn.add_layer(Layer(4, "sigmoid", training_data.shape[1]))
        nn.add_layer(Layer(5, "sigmoid"))
        nn.add_layer(Layer(1, "sigmoid"))

        for i in range(NUM_EPOCHS):
            nn.train(training_data, training_labels, learning_rate=0.03)

        tmp = nn.get_metrics(testing_data, testing_labels, lambda x: np.round(x, 0))
        accuracy[k].append(tmp['accuracy'])
        precision[k].append(tmp['precision'])
        recall[k].append(tmp['recall'])
        f1_score[k].append(tmp['f1 score'])

print("std of accuracy: ", np.array(accuracy).std(axis=0))
print("std of precision: ", np.array(precision).std(axis=0))
print("std of recall: ", np.array(recall).std(axis=0))
print("std of f1_score: ", np.array(f1_score).std(axis=0))

ci_accuracy = np.std(accuracy, axis=0)
ci_precision = np.std(accuracy, axis=0)
ci_recall = np.std(accuracy, axis=0)
ci_f1_score = np.std(accuracy, axis=0)

accuracy = np.array(accuracy).mean(axis=0)
precision = np.array(precision).mean(axis=0)
recall = np.array(recall).mean(axis=0)
f1_score = np.array(f1_score).mean(axis=0)

plt.title("Evolution des mesures en fonction du bruit dans les données d'apprentissage")

plt.subplot(2,2,1)
plt.fill_between(stds, (accuracy-ci_accuracy), (accuracy+ci_accuracy), color='green', alpha=0.1)
plt.plot(stds, accuracy, label='Accuracy', color='green')
plt.xlabel("Ecart-type du bruit")
plt.ylabel('mesure')
plt.xscale('log')
plt.legend()

plt.subplot(2,2,2)
plt.fill_between(stds, (precision-ci_precision), (precision+ci_precision), color='blue', alpha=0.1)
plt.plot(stds, precision, label='Precision', color='blue')
plt.xlabel("Ecart-type du bruit")
plt.ylabel('mesure')
plt.xscale('log')
plt.legend()

plt.subplot(2,2,3)
plt.fill_between(stds, (recall-ci_recall), (recall+ci_recall), color='red', alpha=0.1)
plt.plot(stds, recall, label='Recall', color='red')
plt.xlabel("Ecart-type du bruit")
plt.ylabel('mesure')
plt.xscale('log')
plt.legend()

plt.subplot(2,2,4)
plt.fill_between(stds, (f1_score-ci_f1_score), (f1_score+ci_f1_score), color='yellow', alpha=0.1)
plt.plot(stds, f1_score, label='F1 score', color='yellow')
plt.xlabel("Ecart-type du bruit")
plt.ylabel('mesure')
plt.xscale('log')
plt.legend()

plt.show()


