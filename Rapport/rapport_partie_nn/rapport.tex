\documentclass[pmlr,twocolumn]{jmlr} % W&CP article
% \documentclass[pmlr,twocolumn]{jmlr} % W&CP article
\usepackage{natbib}
\setcitestyle{square, comma, numbers,sort&compress, super}
\input{commands}

\usepackage{booktabs}
\usepackage{float}
\usepackage[load-configurations=version-1]{siunitx} % newer version

\theorembodyfont{\upshape}
\theoremheaderfont{\scshape}
\theorempostheader{:}
\theoremsep{\newline}
\newtheorem*{note}{Note}

\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\LL}{\mathcal{L}}

\title[Short Title]{Étude des réseaux de neurones}

\title{Étude des réseaux de neurones comme outil d'apprentissage supervisé\titletag{}}

\author{%
 \Name{Malik Algelly} \Email{Malik.Algelly@etu.unige.ch}\\
 \Name{Luca Perone} \Email{Luca.Perone@etu.unige.ch}\\
 \Name{Juraj Rosinsky} \Email{Juraj.Rosinsky@etu.unige.ch}\\
 \Name{Samuel Simko} \Email{Samuel.Simko@etu.unige.ch}
}
\begin{document}

\maketitle

\begin{abstract}
Nous proposons une nouvelle librairie de création, modification
et entraînement de réseaux de neurones, perceptrons et classifieurs k-NN, nommée \textbf{battelle}.
Nous utilisons cette librairie pour étudier l'emploi des réseaux de neurones
comme outil d'apprentissage supervisé, à travers la confection de classifieurs
entrainés sur des bases de données connues de UCI (\cite{Dua:2019}). Nous comparons les résultats
obtenus avec d'autres librairies de réseaux de neurones tels que Scikit-learn (\cite{scikit-learn}), ainsi que le classifieur k-NN.
\end{abstract}

\begin{keywords}
Neural Networks, Supervised Learning, Machine Learning, Artificial Intelligence.
\end{keywords}

\section{Introduction}
\label{sec:intro}

Les réseaux de neurones forment la pierre angulaire de l'intelligence artificielle moderne et de l'apprentissage profond.

Des librairies python telles que Scikit-Learn (\cite{scikit-learn}) proposent des interfaces modernes pour construire des réseaux de neurones. Nous allons tout d'abord expliquer les fondements théoriques
derrière l'apprentissage des réseaux de neurones, pour ensuite pouvoir analyser leur performance.

\section{Théorie des Réseaux de Neurones}
\label{sec:1}
\subsection{Définitions et notations}
Soit l'échantillon $x \in \R^n$, auquel est associé le label $y \in \R^m$.

Soit un réseau de neurones possèdant $l$ couches numérotées, ou "layers".
Soient $w_{ji}^{(k)}$ les poids du neurone entre le $i$ème neurone de la couche $k-1$
et le  $j$ième neurone de la couche $k$.
Soient $b_{ji}^{(k)}$ le biais du $i$ème neurone de la couche $k-1$.
Soit $g^{(k)}$ la fonction d'activation associée à la couche $k$.

\subsection{Algorithmes}
Le réseau forme une prédiction grâce à l'algorithme de propagation (\ref{alg:prop}).

\begin{algorithm}[htbp]
\floatconts
  {alg:prop}%
  {\caption{Propagation}}%
{%
  % \item For $k=1$ to maximum number of iterations
    \begin{enumerate}
      \item Pour $n=1$ jusqu'à $l$
        \begin{enumerate}
          \item $h_j^{(n)} := \sum_i w_{ji}^{(n)} x_i^{(n-1)} + b_j^{(n-1)}$
          \item $x_j^{(n)} := g^{(n)}(h_j^{(n)})$
    \end{enumerate}
  \item Retourner le vecteur $x^{(l)}$
\end{enumerate}
}%
\end{algorithm}

Au fur et à mesure des prédictions, le réseau est entrainé grâce à l'algorithme de rétropropagation (\ref{alg:back})
On utilise un facteur $\lambda$, que l'on nomme le "learning rate").

\begin{algorithm}[htbp]
  \floatconts
  {alg:back}%
  {\caption{Rétropropagation}}%
  {%
    \begin{enumerate}
      % \item For $k=1$ to maximum number of iterations
      \item $e_i^{l} := g^{(l)} {'} (h_i^{l})(y_i - x_i^{(l)})$
      \item   \begin{enumerate}
                \item Pour $k=l$ jusqu'à $2$
                  \begin{enumerate}
                    \item \mbox{ $e_j^{(k-1)} := g'^{(k-1)}(h_j^{k-1}) \sum_i w_{ij}^{(k)}e_i^{(k)}$ }
                  \end{enumerate}
                \item Pour $k=1$ jusqu'à $l$
                  \begin{enumerate}
                    \item $w_{ij}^{(l)} = w_{ij}^{(l)} - \lambda e_i^{(l)} x_j^{(l-1)}$
                    \item $b_{ij}^{(l)} = b_{ij}^{(l)} - \lambda e_i^{(l)}$
                  \end{enumerate}
              \end{enumerate}
    \end{enumerate}
  }
\end{algorithm}


Cet algorithme modifie les poids de façon à minimiser la perte, ou "loss", définie comme
\[
\LL := \frac{1}{2} \sum_i (x^{(l)} - y_i)^2
.\]

Avec $y$ le vecteur label, et $x^{(l)}$ le vecteur retournée par l'algorithme de propagation.

\subsection{Explication de l'algorithme de rétropropagation}

Selon le cours (\cite{slides}), on a
\[
\frac{\partial \LL}{\partial w_{ji}^{(l)}} = \frac{\partial \LL}{\partial x_j^{(l)}}\frac{\partial x_j^{(l)}}{\partial h_j^{(l)}}\frac{\partial h_j^{(l)}}{\partial w_{ji}^{(l)}}
.\]

Et

\[
  \frac{\partial h_j^{(l)}}{\partial w_{ji}^{(l)}} = \sum_k w_{jk}^{(l)} \frac{\partial x_k^{(l-1)}}{\partial w_{ji}^{(l)}} + b_j^{(l)} = x_j^{(l-1)}
.\]

\[
  \frac{\partial \LL}{\partial x_j^{(l)}} = (x_j^{(l)} - y_j)
.\]

\[
  \frac{\partial x_j^{(l)}}{\partial h_j^{(l)}} = g'^{(l)}(h_j^{(l)})
.\]

Donc, par définition de  \\
$e_i^{l} := g^{(l)}{'}(h_i^{l})(y_i - x_i^{(l)})$,

\[
\frac{\partial \LL}{\partial w_{ji}^{(l)}} = e_j^{(l)} x_j^{(l-1)}
.\]

% \vspace*{1cm}

Remarquons que $$\frac{\partial h_j^{(l)}}{\partial b_{j}^{(l)}} = e_j^{(l)}$$\\

car $$\frac{\partial h_j^{(l)}}{\partial b_j^{(l)}} = 1$$.\\

Par principe de descente en gradient, nous pouvons ainsi soustraire
une valeur proportionnelle à
$e_j^{(l)} x_j^{(l-1)}$ au poids $w_{ji}^{(l)}$,
et faire ainsi baisser la perte $\LL$.\\

Pour les couches inférieures, remarquons que

\begin{equation} \label{eq:b1}
  \frac{\partial \LL}{\partial w_{ji}^{(l)}} = \frac{\partial h_j^{(l)}}{\partial w_{ji}^{(l)}}\frac{\partial \LL}{\partial h_j^{(l)}} = x_i^{(l-1)} \frac{\partial \LL}{\partial h_j^{(l)}}
\end{equation}

Et

\begin{equation} \label{eq:b2}
  \frac{\partial \LL}{\partial h_j^{(l)}} = \frac{\partial x_j^{(l)}}{\partial h_j^{(l)}} \frac{\partial \LL}{\partial x_j^{(l)}}
  = g'^{(l)}(h_j^{(l)}) \frac{\partial \LL}{\partial x_j^{l}}
\end{equation}

Et finalement

\begin{equation} \label{eq:b3}
\frac{\partial \LL}{\partial x_j^{(l)}} = \sum_k \frac{\partial h_k^{(l-1)}}{\partial x_j^{(l)}}\frac{\partial \LL}{\partial h_k^{(l-1)}} = \sum_k w_{kj}^{(l)} \frac{\partial \LL}{\partial h_k^{(l-1)}}
\end{equation}

On peut donc calculer les gradients $\frac{\partial \LL}{\partial w_{jk}^{(n)}}$ pour toutes les couches,
en chaînant les équations \ref{eq:b2} et \ref{eq:b3} pour calculer les gradients itérativement pour
$(l-1), (l-2), \ldots$

En remplaçant avec $$e_j^{(k-1)} := g'^{(k-1)}(h_j^{k-1}) \sum_i w_{ij}^{(k)}e_i^{(k)}$$

On tombe sur

\[
\frac{\partial \LL}{\partial w_{ji}^{(k)}} = x_i^{(l-1)} e_j^{k}
\]

Car $e_j^{(l)} = \frac{\partial \LL}{\partial h_j^{(l)}}$.

Pour les gradients en fonction des biais, on peut oublier le facteur $x_i^{l-1}$
$\frac{\partial h_j^{(l)}}{\partial b_j^{(l)}} = 1$.\\

L'algorithme \ref{alg:back} va donc correctement entraîner les poids et bias.


\section{Implémentation}

Nous avons développé, dans le langage Python, une librairie de réseaux de neurones,
nommée \textbf{Battelle}
conçue pour faciliter la construction de réseaux simples.
La syntaxe de création de réseaux a été inspirée de celle de Keras (\cite{Chollet:2015}).

L'utilisateur peut spécifier le nombre de couches, ainsi que le nombre de neurones de chaque couche.
Il peut également choisir la dimension d'entrée du réseau, ainsi que la fonction d'agrégation utilisée
au sein de chaque couche (entre la fonction sigmoïde et la fonction tanh). Il peut aussi définir la sienne.

Il peut ensuite entrainer son réseau grâce à l'algorithme de rétropropagation, et effectuer
des mesures de performance avec quatre types de métriques (accuracy, recall, precision, F1-score).

\textbf{Battelle} possède trois sous-modules:

\begin{itemize}
  \item \textbf{Battelle.nn}, pour le réseaux de neurones;
  \item \textbf{Battelle.knn}, pour le classifieur k-nn;
  \item \textbf{Battelle.perceptron}, pour le perceptrons.
\end{itemize}

Une documentation a été crée et est disponible sur le site \url{battelle.readthedocs.io}.

% La librairie a été archivée sur le Python Packaging Index (PyPI).


\input{4}
\input{5}
\newpage

\section{Impact de la complexité du réseau sur l'apprentissage}
\label{sec:7}

Nous avons entrainé quatre réseaux de neurones
avec un taux d'apprentissage de 0.04 et la fonction d'agrégation tanh à l'aide des données représentées sur la figure \ref{fig:7_data}

\begin{figure}[htpb]
  \centering
  \includegraphics[width=0.5\textwidth]{images/7_data.png}
  \caption{Données d'entraînement}
  \label{fig:7_data}
\end{figure}

Nous avons mesuré la précision (Precision), le rappel (Recall), l'accuracy et le F1-score
entre chaque Epoch de l'entraînement, pour des réseaux possèdant 3 couches avec un nombre de neurones
variable. Nous notons $[x, y]$ le réseau possèdant 3 couches de $x, y$ et $1$ neurone respectivement.
5 mesures ont été prises pour des réseaux $[1, 1]$, $[2, 2]$, $[4, 4]$, $[8, 8]$ et $[16, 16]$.

\begin{figure*}
  \centering
  \includegraphics[width=\textwidth]{images/complexity.png}
  \caption{Évolution des mesures pour des réseaux de complexité variable}
  \label{fig:comp}
\end{figure*}

Sur la figure \ref{fig:comp}, nous avons représenté l'écart-type des mesures effectuées
pour chaque réseau avec des aires de couleur.
Nous pouvons apercevoir que le réseau $[1, 1]$ n'a pas su s'améliorer après la première epoch.
Il classifie toutes les données dans la classe la plus présente, et obtient donc une accuracy
de $0.8$.

Nous remarquons que plus le réseau est complexe, plus les métriques s'améliorent rapidement.
Un réseau complexe s'adapte plus rapidement à nos données.

Il obtient également des meilleures performances. Par exemple, le réseau $[2, 2]$ obtient une
accuracy finale de  $0.89 \pm 0.02$, tandis que le réseau $[16, 16]$ obtient une accuracy finale
de $0.97 \pm 0.02$.

Les écarts-types des valeurs mesurées pour chaque réseau sont également moins importantes pour de plus grands réseaux.
\input{10}
\input{11}

\newpage

\section{Contributions}

\begin{itemize}
  \item
\textbf{Malik Algelly}: développement (battelle.nn, battelle.perceptron, créations de scripts pour battelle.nn, battelle.perceptron), écriture du rapport (sections \ref{sec:5} ,\ref{sec:6})

  \item
\textbf{Luca Perone}: développement (battelle.nn, battelle.knn, créations de tests, de scripts et d'exemples pour battelle et sklearn), structure de la librairie battelle.

  \item
\textbf{Juraj Rosinsky}: développement (méthodes de la librairie battelle.nn, scripts d'entraînements pour battelle.nn et sklearn, scripts de visualisation de données), écriture du rapport (sections \ref{sec:4}, \ref{sec:10}, \ref{sec:11})

  \item
\textbf{Samuel Simko}: développement (battelle.nn, scripts d'entraînements pour battelle et sklearn), structure de la librairie battelle, documentation en ligne, écriture du rapport (sections \ref{sec:1}, \ref{sec:7}, \ref{sec:10})

\end{itemize}

\section{Citations and Bibliography}
\label{sec:cite}

\acks{Nous remercions le corps enseignant du cours \textit{Intelligence Artificielle} de l'Université de Genève pour l'aide apportée.}

\newpage
\bibliography{jmlr-sample}

\appendix

\end{document}
